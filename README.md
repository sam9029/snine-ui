# Snine-UI
S9-UI or Snine-UI

【[Snine-Ui-源码地址--GitHub](https://github.com/sam9029/snine-ui)】

【[Snine-Ui-源码地址--Gitee](https://gitee.com/sam9029/snine-ui)】

# 组件库搭建与发布
> 详情见 [base_init_project](./base_init_project.md)

# 组件规划
- Icon **✔**
- Button **✔**
- tag **✔**
- Upload **UN** --hard
- tabs
- Card
- Cascader
- Switch
- Table
- Form
- Input **✔**
- Radio **UN** --hard
- Checkbox
- Select **UN** --hard

# 起因

- 不想一直原地踏步，一直是菜鸡